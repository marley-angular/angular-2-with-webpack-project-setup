import 'core-js';
import 'reflect-metadata';
import 'zone.js/dist/zone';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { AppComponent } from './app/app.component';
import { AppModule } from './app.module';

// platformBrowserDynamic().bootstrapModule(AppComponent);

const platform = platformBrowserDynamic();
platform.bootstrapModule(AppModule);
